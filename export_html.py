#!/usr/bin/env python3

from sys import argv
from sys import stderr
from os.path import isfile
from os import getcwd

def contrôle_arguments():
    for i in range(1, len(argv)):
        if not isfile(argv[i]):
            print('Utilisation :', argv[0], '<fichier1> <fichier2> ...', file=stderr)
            exit(1)

def écrire_début_fichier(titre, fichier_sortie):
    fichier_sortie.write('<!DOCTYPE html>\n')
    fichier_sortie.write('<html>\n')
    fichier_sortie.write('  <head>\n')
    fichier_sortie.write('    <title>' + titre + '</title>\n')
    fichier_sortie.write('    <meta charset="utf-8" />\n')
    fichier_sortie.write('  </head>\n')
    fichier_sortie.write('  <body>\n')

def écrire_fin_fichier(fichier_sortie):
    fichier_sortie.write('  </body>\n')
    fichier_sortie.write('</html>\n')

def écrire_corps(fichier_entrée, fichier_sortie):
    while True:
        position = fichier_entrée.tell()
        ligne = fichier_entrée.readline()
        if ligne == '':
            break
        fichier_entrée.seek(position)

        fichier_sortie.write('    <p>')
        fin_paragraphe = False
        dans_entité = False
        dernière_étiquette = 'O'
        while not fin_paragraphe:
            # Lecture d’une ligne dans chaque fichiers.
            ligne = fichier_entrée.readline()

            # Vérification de fin de paragraphes et de conformité des
            # lignes.
            if ligne == '':
                fin_paragraphe = True
                continue
            elif ligne == '\n':
                continue

            if len(ligne.split()) < 3:
                continue
            prédiction = ligne.split()[2]

            pas_une_entité = True
            if prédiction != 'O':
                pas_une_entité = False
                dans_entité = True

            if not pas_une_entité and dernière_étiquette != prédiction[2:]\
               and dernière_étiquette != 'O':
                fichier_sortie.write(' [' + dernière_étiquette + ']')

            if pas_une_entité and dans_entité:
                dans_entité = False
                fichier_sortie.write(' [' + dernière_étiquette + ']')

            if prédiction != 'O':
                dernière_étiquette = prédiction[2:]
            else:
                dernière_étiquette = prédiction

            fichier_sortie.write(' <span title="' + ligne.split()[1] + '"')
            if dans_entité:
                if prédiction != ligne.split()[1]:
                    fichier_sortie.write(' style="color: #ff0000;"')
                else:
                    fichier_sortie.write(' style="color: #00ff00;"')
            elif ligne.split()[1] != 'O':
                # Il y a des erreurs dans les fichiers parfois, à supprimer.
                if ligne.split()[1][:2] == 'B-' or ligne.split()[1][:2] == 'I-':
                    fichier_sortie.write(' style="color: #ff00ff;"')

            fichier_sortie.write('>')
            if ligne.split()[0] == '.':
                fin_paragraphe = True
            fichier_sortie.write(' ' + ligne.split()[0])
            fichier_sortie.write('</span>')


        fichier_sortie.write('</p>\n')

def créer_fichier_prédictions(fichier_entrée, fichier_sortie):
    écrire_début_fichier(fichier_entrée.name, fichier_sortie)
    écrire_corps(fichier_entrée, fichier_sortie)
    écrire_fin_fichier(fichier_sortie)

def principale():
    contrôle_arguments()

    fichiers = []
    for i in range(1, len(argv)):
        if argv[i][0] != '/':
            créer_fichier_prédictions(open(getcwd() + '/' + argv[i], 'r'),
                                      open(getcwd() + '/' + argv[i] +
                                           '.html', 'w'))
        else:
            créer_fichier_prédictions(open(argv[i], 'r'),
                                      open(argv[i] + '.html', 'w'))


principale()
